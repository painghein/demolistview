/**
 * Copyright 2012 Gajah Media Pte,Ltd

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package com.gajah.listview.demo;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gajah.listview.adapter.AdapterPaginatedList;
import com.test.listview.demo.R;

public class TestListViewDemoActivity extends Activity {

	ArrayList<HashMap<String, Object>> mdata;
	TextView mPagenum;
	int pos = -1;
	ListView lv;
	private AdapterPaginatedList adapter;
	private final int MSG_UPDATE_PAGE_NUM = 1;
	private final int MSG_DO_ITEM_CLICK = 2;
	private final int VIEW_COUNT = 10;

	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);

			switch (msg.what) {
			case MSG_UPDATE_PAGE_NUM:
				mPagenum.setText((CharSequence) msg.obj);
				break;
			case MSG_DO_ITEM_CLICK:
				Toast.makeText(getApplicationContext(), (CharSequence) msg.obj,
						Toast.LENGTH_SHORT).show();
				break;
			}
		}

	};

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.test_demo);

		mPagenum = (TextView) findViewById(R.id.test_pagenum);
		mdata = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> item;

		for (int i = 0; i < 50; i++) {
			item = new HashMap<String, Object>();
			item.put("index", i + "--Gajah::" + i);
			mdata.add(item);
		}
		lv = (ListView) findViewById(R.id.test_listview);
		adapter = new AdapterPaginatedList(this, mdata,
				R.layout.test_demo_list_item, new String[] { "index" },
				new int[] { R.id.text }, VIEW_COUNT);
		lv.setAdapter(adapter);
		lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		lv.clearChoices();
		lv.post(new Runnable() {
			public void run() {
				lv.setSelection(lv.getFirstVisiblePosition());
				Object o = 1 + "/" + (adapter.getTotalpage());
				sendMessage(MSG_UPDATE_PAGE_NUM, o);
			}
		});

		lv.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				int index = adapter.getIndex();
				if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT
						&& event.getAction() == KeyEvent.ACTION_DOWN) {
					if (index-- > 1) {
						adapter.setIndex(index);
						adapter.notifyDataSetChanged();
						Object o = index + "/" + (adapter.getTotalpage());
						sendMessage(MSG_UPDATE_PAGE_NUM, o);
					}
					return true;
				} else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT
						&& event.getAction() == KeyEvent.ACTION_DOWN) {
					if (index++ < mdata.size() / adapter.VIEW_COUNT) {
						adapter.setIndex(index);
						adapter.notifyDataSetChanged();
						Object o = index + "/" + (adapter.getTotalpage());
						sendMessage(MSG_UPDATE_PAGE_NUM, o);
					}
					return true;
				}
				return false;
			}
		});

		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				// Find out actual position from adapter
				int actual_index = ((adapter.getIndex() - 1) * VIEW_COUNT)
						+ position;
				Object o = actual_index + " is clicked";
				sendMessage(MSG_DO_ITEM_CLICK, o);
			}

		});
	}

	/**
	 * 
	 * @param msgCode
	 * @param o
	 */
	public void sendMessage(int msgCode, Object o) {
		Message m = new Message();
		m.what = msgCode;
		m.obj = o;
		mHandler.sendMessage(m);
	}

}